<?php

use Illuminate\Support\Facades\Artisan;

Artisan::command('bot:start', function () {
    try {
       $credential = new App\Config\Credential;
       $config     = new App\Config\Config;
    } catch (\Throwable $t) {
        $this->warn('Startup failed: ' . $t->getMessage());
        return 0;
    }
    $instance = new \App\Bot\Kernel(
        $credential->application->token,
        $config
    );
    $instance->init()->run();
});
