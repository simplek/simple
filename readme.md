# Simple Bot

`simplek/simple` `PHP` `Discord`

The easy-to-use Discord bot for Simple K community.

Based on [teamreflex/DiscordPHP](https://github.com/teamreflex/DiscordPHP) and [laravel/laravel](https://github.com/laravel/laravel).


## Run

`artisan bot:start`


## Requirements

PHP >= 7.3

BCMath Extension

Ctype Extension

Fileinfo Extension

JSON Extension

Mbstring Extension

OpenSSL Extension

PDO Extension

Tokenizer Extension

XML Extension

Zlib Extension
