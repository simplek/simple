<?php
namespace App\Bot;

use Illuminate\Support\Str;
use Commands\AliasMap as AliasSource;

class CommandEngine
{
    protected $config;
    protected $clientId;
    protected $prefix;
    protected $preservedAction = [];

    protected $aliasMap;

    protected function shouldRun($message)
    {
        $authorId = $message->author->id;
        if ($authorId === $this->clientId) return false;
        if (! Str::startsWith($message->content, $this->prefix)) return false;

        return true;
    }
    protected function dealWithCommand($message)
    {
        $parts = $this->parseCommandText($message->content);
        if (count($parts) <= 1) return false;

        $action = $parts[1];
        $action = strtolower(trim($action, '"\/.\'`~'));
        if (in_array($action, $this->preservedAction)) return false;


        $callable = 'Commands\\'.ucfirst($action);
        if (! class_exists($callable)) return false;
        $callable = new $callable;
        if (! is_callable($callable)) return false;

        return $callable($message);
    }

    protected function parseCommandText($text)
    {
        return preg_split('/\s(?=([^"]*"[^"]*")*[^"]*$)/', $text);
    }
    public function filter($message)
    {
        if (! $this->shouldRun($message)) return false;
        return $this->dealWithCommand($message);
    }
    public function __construct($config)
    {
        $this->config   = $config;
        $this->clientId = $config->application->clientId;
        $this->prefix   = $config->command->prefix;
        $this->aliasMap = new AliasSource;
    }
}
