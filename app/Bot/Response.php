<?php
namespace App\Bot;

class Response
{
    protected $content = '';

    public function getContent()
    {
        return $this->content;
    }

    public function __construct($content)
    {
        $this->content = $content;
    }
}
