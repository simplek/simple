<?php
namespace App\Bot;
use Discord\Discord;
use Discord\Parts\Channel\Message;

class Kernel
{
    protected $handle = null;
    protected $config = null;

    public function getHandle()
    {
        return $this->handle;
    }
    public function init()
    {
        $handle = $this->getHandle();
        $engine = new CommandEngine($this->config);
        $handle->on('ready', function ($discord) use ($engine) {
            $discord->on('message', function (Message $message) use ($engine) {
                $result = $engine->filter($message);

                if ($result instanceof Response) {
                    return $message->channel->sendMessage($result->getContent());
                }
            });
        });
        return $handle;
    }
    public function __construct($token, $config)
    {
        $this->handle = new Discord([
            'token' => $token
        ]);
        $this->config = $config;
    }
}
