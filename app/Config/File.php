<?php
namespace App\Config;
use App\DataStructure\AccessSafeObject;
use Symfony\Component\Yaml\Yaml;

class File
{
    protected $fullFilename = null;
    protected $document = null;

    public function getDefaultPath($append = '')
    {
        return base_path($append);
    }
    public function getAcceptedExtension()
    {
        return ['.yaml', '.yml'];
    }
    public function resolveFilenameAttempt($filename)
    {
        if (is_file($filename)) return $filename;
        $f = $this->getDefaultPath($filename);
        if (is_file($f)) return $f;

        return null;
    }
    public function resolveFilename($filename)
    {
        foreach (array_merge([''], $this->getAcceptedExtension()) as $ext) {
            $attempt = $this->resolveFilenameAttempt($filename.$ext);
            if (! is_null($attempt)) return $attempt;
        }
        throw new FileNotFoundException;
    }
    public function getObject()
    {
        return new AccessSafeObject($this->document);
    }
    public function get($key, $convert = true)
    {
        return $this->getObject()->get($key, $convert);
    }
    public function __get($key)
    {
        return $this->get($key);
    }
    public function __construct($filename)
    {
        $this->fullFilename = $this->resolveFilename($filename);

        try {
            $this->document = Yaml::parseFile($this->fullFilename);
        } catch (\Throwable $t) {
            throw new FileNotParsableException('', 0, $t);
        }
    }
}
