<?php
namespace App\DataStructure;

class Map
{
    protected $data = [];

    public function has($key)
    {
        return isset($this->data[$key]);
    }

    public function get($key)
    {
        return $this->data[$key] ?? null;
    }
}
