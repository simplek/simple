<?php
namespace App\DataStructure;

class AccessSafeObject
{
    protected $data;

    public function get($key, $convert = true)
    {
        $value = $this->data[$key] ?? null;
        if (is_null($value)) return null;

        if ($convert) {
            if ($value instanceof \stdClass) return new AccessSafeObject($value);
            if (is_array($value)) {
                if (empty($value)) return $value;
                if (isset($value[0])) return $value;
                return new AccessSafeObject($value);
            }
        }

        return $value;
    }
    public function __get($key)
    {
        return $this->get($key);
    }
    public function __construct($data = [])
    {
        if (is_object($data)) $data = (array)$data;
        $this->data = $data;
    }
}
